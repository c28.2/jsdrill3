// Q3 Find all users with masters Degree.

function problem3(users){
    console.log("Users with master degree");
    let arrayDegree=[];
    for(let key in users){
        if(users[key].qualification == "Masters"){
            arrayDegree.push(key);
        }
    }
    if(arrayDegree.length >0){
        return arrayDegree;
    }
    else{
        return "No one with masters degree";
    }
}

module.exports = problem3;