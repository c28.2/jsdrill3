// Q1 Find all users who are interested in playing video games.

function problem1(users){
    let game=[];
    let d={}
    for(let key in users){
        d[key]=users[key].interests;
    }
    for(let keys in d){
        let games=d[keys];
        for(let index = 0 ;index < games.length ; index++){
            if(games[index] == "Video Games"){
                game.push(keys);
            }        
        }
    }
    if(game.length>0){
        return game;
    }
    else{
        return "No one interested in video games";
    }
}


module.exports = problem1;