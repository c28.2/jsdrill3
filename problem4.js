// Q4 Group users based on their Programming language mentioned in their designation.

function problem4(users){
    let object={};
    for(let key in users){
        object[key]=users[key].desgination;
    }
    let groups = {};
    for(let key in object) {
        let designation = object[key];
        let userName = key;
        if (designation in groups) {
            groups[designation].push(userName);
        } else {
            groups[designation] = [userName];
        }
    }
    console.log(groups);
}

module.exports = problem4;

