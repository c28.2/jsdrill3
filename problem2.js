// Q2 Find all users staying in Germany.

function problem2(users){
    let country=[];
    for(let key in users){
        if(users[key].nationality == "Germany"){
            country.push(key);  
        }
    }
    if(country.length>0){
        return country;
    }
    else{
        return "No one staying in germany";
    }
}

module.exports = problem2;